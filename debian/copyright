Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plasma-gamemode
Upstream-Contact: Harald Sitter <sitter@kde.org>
Source: https://invent.kde.org/sitter/plasma-gamemode

Files: *
Copyright: 2020-2022, Harald Sitter <sitter@kde.org>
License: GPL-2.0-only_OR_GPL-3.0-only_OR_LicenseRef-KDE-Accepted-GPL

Files: CMakeLists.txt
       .gitlab-ci.yml
       src/applet/CMakeLists.txt
       src/CMakeLists.txt
       src/Messages.sh
Copyright: 2020-2022, Harald Sitter <sitter@kde.org>
License: BSD-3-Clause

Files: picture.png.license
       README.md
       src/applet/metadata.desktop
       src/com.feralinteractive.GameMode.Game.xml
       src/com.feralinteractive.GameMode.xml
       src/org.freedesktop.DBus.Properties.xml
       src/qmldir
Copyright: 2022, Harald Sitter <sitter@kde.org>
License: CC0-1.0

Files: logo.png.license
Copyright: 2016, andreas kainz <kainz.a@gmail.com>
License: LGPL-3.0-only_OR_LicenseRef-KDE-Accepted-LGPL

Files: debian/*
Copyright: 2022, Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
License: GPL-2.0-only_OR_GPL-3.0-only_OR_LicenseRef-KDE-Accepted-GPL

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or
 without modification, are permitted provided that the
 following conditions are met:
 .
 1. Redistributions of source code must retain the above
    copyright notice, this list of conditions and the following
    disclaimer.
 .
 2. Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials
    provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products
    derived from this software without specific prior written
    permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated
 all copyright and related and neighboring rights to this
 software to the public domain worldwide. This software is
 distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain
 Dedication along with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain
 Dedication can be found in `/usr/share/common-licenses/CC0-1.0’.

License: GPL-2.0-only_OR_GPL-3.0-only_OR_LicenseRef-KDE-Accepted-GPL
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 3 of
 the license or (at your option) at any later version that is
 accepted by the membership of KDE e.V. (or its successor
 approved by the membership of KDE e.V.), which shall act as a
 proxy as defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2', likewise, the complete text
 of the GNU General Public License version 3 can be found in
 `/usr/share/common-licenses/GPL-3'.
 
License: LGPL-3.0-only_OR_LicenseRef-KDE-Accepted-LGPL
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the license or (at your option) any later version
 that is accepted by the membership of KDE e.V. (or its successor
 approved by the membership of KDE e.V.), which shall act as a
 proxy as defined in Section 6 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 3 can be found in
 `/usr/share/common-licenses/LGPL-3'.
